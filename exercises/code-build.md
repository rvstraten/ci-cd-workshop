# Code build and package validation

Code build is converting the source code into an distributable binary artifact.

## Goals

Creating a distributable binary is converting source code into an binary that is eligible for deployment and validating 
if no insecure third party dependencies are used.

## Approach

Because this is node.js application there is no need for actual compilation. But to make distributable a zip file is 
created.

For auditing third party dependencies the **npm audit** command is used. [NPM audit](https://docs.npmjs.com/getting-started/running-a-security-audit)

## Exercise

In this exercise the building is done by creating a zip that can distributed through an artifact store and can be used 
with a deployment.

### Step 1: The build stage

Create a build_zip job and build stage will be the first step in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build

# The build job
build_zip:
  stage: build
  before_script: []
  image: brandography/alpine-zip
  script:
    - zip lambda src/index.js
  artifacts:
    paths:
    - lambda.zip
    expire_in: 1 week
```

The stage **build** is an new logical divider of steps within the continuous delivery pipeline. The job **build_zip** is 
actual command that starts the building (bundling) of the application. 


### Step 2: Validate the packages

Security validation of third packages is also important to validate if no vulnerabilities are imported within the 
application. This can be done with **npm audit** this will validate whether the dependencies are save to be used.

Add the following to the [.gitlab-ci.yml](../.gitlab-ci.yml)

```yaml
security_audit:
  stage: build
  before_script: []
  script:
  - npm audit
```

